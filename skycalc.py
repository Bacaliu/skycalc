#!/usr/bin/env python
# coding: utf-8

# ███████╗██╗  ██╗██╗   ██╗ ██████╗ █████╗ ██╗      ██████╗
# ██╔════╝██║ ██╔╝╚██╗ ██╔╝██╔════╝██╔══██╗██║     ██╔════╝
# ███████╗█████╔╝  ╚████╔╝ ██║     ███████║██║     ██║
# ╚════██║██╔═██╗   ╚██╔╝  ██║     ██╔══██║██║     ██║
# ███████║██║  ██╗   ██║   ╚██████╗██║  ██║███████╗╚██████╗
# ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝╚═╝  ╚═╝╚══════╝ ╚═════╝

import os
import sys
from datetime import datetime, timedelta
from math import log10, sin, cos, pi
import numpy as np
import matplotlib.pyplot as plt
import requests
# import skyfield
from skyfield.api import wgs84, Loader
from skyfield.framelib import ecliptic_frame
# from skyfield.data import hipparcos, stellarium
from skyfield.magnitudelib import planetary_magnitude
# from skyfield.trigonometry import position_angle_of
# from skyfield.units import Angle
from skyfield import almanac, searchlib
from tqdm.auto import tqdm

PATH = os.path.abspath(os.path.dirname(sys.argv[0]))
load = Loader(f"{PATH}/data")
ts = load.timescale()
SPACE = "&nbsp;"
EPH = load('de421.bsp')

# Herunterladen der Satellitendaten | Downloading satellite-data
SAT_URL = 'https://celestrak.com/NORAD/elements/visual.txt'
VISUAL = load.tle_file(SAT_URL, reload=False)
# Falls die Daten älter als ein Tag sind, wird neu geladen
delta_t = datetime.now().astimezone() - VISUAL[0].epoch.utc_datetime()
if delta_t > timedelta(days=1):
    VISUAL = load.tle_file(SAT_URL, reload=True)


###############################################################################
# RECHENHILFE
###############################################################################

def mix(num: float, n_min: float, n_max: float) -> float:
    """
    Gibt den Wert von num zurück, aber mindestens n_min und höchstens n_max.
    """
    return max(n_min, min(num, n_max))


def position(lat: float, lon: float, elev: float):
    """
    Gibt bei gegebenen Koordinaten den Ort zurück.
    Das Tuble enthält den geozentrischen und den heliozentrsichen Ort.
    """
    ort_g = wgs84.latlon(lon, lat, elev)
    ort_h = ort_g+EPH["EARTH"]
    return ort_g, ort_h


def ptime(dt: datetime) -> str:
    """returns the String of a not timezone-aware datetime object"""
    return dt.astimezone().strftime("%Y-%m-%d<br>%Hh%Mm%Ss")


def runde(zahl: float, n: int, m: int = 0) -> str:
    # rundet die Zahl zahl auf n stellen und
    # füllt mit Leerzeichen auf m Stellen auf.
    # round the number zahl to n digits and
    # fills with spaces to at least m digits.
    if n == 0:
        ret = str(int(zahl))
    else:
        ret = str(round(zahl, n))
    ret = max(m-len(ret), 0)*SPACE + ret
    return ret


def offset_mag(ts0, satellite, lon: float, lat: float, elev: float) -> float:
    ort_g, ort_h = position(lon, lat, elev)
    sun = (EPH["EARTH"]+satellite).at(ts0).observe(EPH["SUN"])
    g = sun.separation_from((EPH["EARTH"]+satellite).at(ts0).observe(ort_h))
    d = (satellite-ort_g).at(ts0).altaz()[2]
    return (- 15 + 5*log10(d.km) - 2.5*log10(sin(g.radians)
            + (pi-g.radians)*cos(g.radians)))


def mag(ts0, sat, lon: float, lat: float, elev: float):
    offset = offset_mag(ts0, sat, lon, lat, elev)
    satid = sat_id(sat)
    # ist schon im cache?
    if os.path.exists(f"{PATH}/data/satmag.txt"):
        with open(f"{PATH}/data/satmag.txt", "r") as f:
            for line in f:
                if line.split(":")[0] == satid:
                    m = line.split(":")[1].strip()
                    if m.replace(".", "", 1).isnumeric():
                        return float(m)+offset
                    else:
                        return "?"
        # nicht im cache.
    url = f"http://heavens-above.com/satinfo.aspx?satid={satid}"
    resp = requests.get(url)
    try:
        r = resp.text.split("(Magnitude)")[1][10:].split(" ")[0]
        m = float(r)
    except:
        m = "?"
    with open(f"{PATH}/data/satmag.txt", "a") as f:
        f.write(f"{satid}:{m}\n")
    if isinstance(m, float):
        return m+offset
    else:
        return "?"


def rich(alt: float) -> str:
    for d, n in zip(
        np.arange(0, 360, 22.5),
        [
            "N", "NNO", "NO", "ONO",
            "O", "OSO", "SO", "OSO",
            "S", "SSW", "SW", "WSW",
            "W", "WNW", "NW", "NNW"
        ]
    ):
        if abs((alt % 360)-d) < 11.25:
            return n+("~"*(3-len(n)))
    return "N~~"


def AltAzRaDecDis(eph, ts0, lon: float, lat: float, elev: float):
    ort_g, ort_h = position(lon, lat, elev)
    alt, az, dis = ort_h.at(ts0).observe(eph).apparent().altaz()
    ra, dec, _ = ort_g.at(ts0).from_altaz(alt, az).radec()

    return alt, az, ra, dec, dis


def planetenphase(ts0, pname: str) -> float:
    sun = EPH["EARTH"].at(ts0).observe(EPH["SUN"])
    _, slon, _ = sun.apparent().frame_latlon(ecliptic_frame)
    planet = EPH["EARTH"].at(ts0).observe(EPH[pname])
    _, plon, _ = planet.apparent().frame_latlon(ecliptic_frame)
    phase = (plon.degrees - slon.degrees) % 360.0
    return phase

###############################################################################
# DARSTELLUNG
###############################################################################

##
# ALLGEMEIN
##


def html_row(a: str, b: str, c: str) -> str:
    return (f'<tr><td style="text-align:center">{a}</td>'
            f'<td style="text-align:center;">{b}</td>'
            f'<td>{c}</td></tr>').replace("~", SPACE)


def big_emoji(emoji: str) -> str:
    return f'<span style = "font-size:200%">{emoji}</span><br>'


def satcol(ts0, satellite, lon: float, lat: float, elev: float) -> tuple:
    if not satellite.at(ts0).is_sunlit(EPH):
        return (0, 0, 0.25)

    m = mag(ts0, satellite, lon, lat, elev)
    if isinstance(m, float):
        return (
            (mix(m, 0, 6)/6)**0.5,
            (mix(6-m, 0, 6)/6)**0.5,
            mix(0-m, 0, 4)/4
        )
    else:
        return (0.5, 0.5, 0.5)


##
# SONNE
##


def sun_emoji(helligkeit: int) -> str:
    loe = ["&#x1F30C;", "&#x1F303;", "&#x1F306;",
           "&#x1F307;", "&#x2600;&#xFE0F;"]
    return big_emoji(loe[helligkeit])


def sun_darstell(helligkeit: int) -> str:
    if helligkeit == 3:
        name = "Sun"
    else:
        name = "Dämmerung"
    return (f'{sun_emoji(helligkeit)}'
            f'<a href="{planet_url("sun")}" '
            f'target="_blank">{name}')
##
# MOND
##


def moon_emoji(phase: float) -> str:
    symb = [
            "&#127761;", "&#127762;", "&#127763;", "&#127764;",
            "&#127765;", "&#127766;", "&#127767;", "&#127768;"
           ]
    for s, p in zip(symb, range(0, 360, 45)):
        if abs((phase-p)) < 22.5:
            return big_emoji(s)
    return big_emoji(symb[0])


def moon_url() -> str:
    return "https://theskylive.com/moon-info"


def moon_darstell(ts) -> str:
    return (f'{moon_emoji(planetenphase(ts, "Moon"))}'
            f'<a href="{moon_url()}" target = "_blank">Moon</a>')
##
# PLANETEN
##


def planet_url(name: str) -> str:
    return f"https://theskylive.com/{name.split('_')[0].lower()}-info"


def planet_emoji(name: str) -> str:
    s = {"mercury": "&#x263F;",
         "venus": "&#x2640;",
         "mars": "&#x2642;",
         "jupiter": "&#x2643",
         "saturn": "&#x2644",
         "uranus": "&#x26E2",
         "neptune": "&#x2646"}
    return (f'<span style="font-size:200%;color:{planet_col(name)}"'
            ''+big_emoji(s[name.split('_')[0].lower()])+'</span>')


def planet_darstell(name: str) -> str:
    return (f'{planet_emoji(name)}<a href ="{planet_url(name.lower())}" '
            f'target="_blank">{name.split("_")[0].capitalize()}</a>')
##
# SATELLITEN
##


def sat_id(sat) -> str:
    tar_n = sat.target_name.split("#")[1]
    satid = ""
    for s in tar_n:
        if s == " ":
            break
        satid += s
    return satid


def sat_url(sat) -> str:
    return f"http://heavens-above.com/satinfo.aspx?satid={sat_id(sat)}"


def sat_emoji() -> str:
    return big_emoji("&#128752;")


def sat_darstell(sat) -> str:
    return (f'{sat_emoji()}<a href="{sat_url(sat)}"'
            f'target="_blank">{sat.name}</a>')


def planet_col(name: str) -> str:
    pc = {
        "Sun": "#fa0",
        "Moon": "#888",
        "Mercury": "#840",
        "Venus": "#88f",
        "Mars": "#f40",
        "Jupiter": "#cc8",
        "Saturn": "#884",
        "Uranus": "#484",
        "Neptune": "#44c"
    }
    return pc[name.capitalize().split("_")[0]]

###############################################################################
# TAGEBOGEN
###############################################################################


def plot_tagebogen(ts0, ts1, lon: float, lat: float, elev: float,
                   name: str) -> None:
    plt.rcParams['figure.figsize'] = [16, 6]

    fig = plt.figure()
    ax = fig.add_subplot()

    gridtick = timedelta(hours=1)
    tick = timedelta(minutes=6)
    dt0 = ts0.utc_datetime().astimezone()
    dt1 = ts1.utc_datetime().astimezone()
    dtrange = [
        dt0.astimezone() + i * tick for i in range(1+int((dt1-dt0)/tick))
              ]
    trange = ts.from_datetimes(dtrange)

    ort_g, ort_h = position(lon, lat, elev)

    pb = tqdm(total=(len(trange)-1)*9, desc="Planeten")
    for pname in [
        "Sun", "Moon", "Mercury", "Venus", "Mars", "Jupiter_barycenter",
        "Saturn_barycenter", "Uranus_barycenter", "Neptune_barycenter"
                 ]:
        col = planet_col(pname)
        planet = EPH[pname]
        fname = pname.split("_")[0]
        m = []
        pb.write("\r"+pname, end="    ")
        for i, t in enumerate(trange[:-1]):
            pb.update()
            if i == 0:
                continue
            AZ = ort_h.at(t).observe(planet).apparent().altaz()[1].degrees

            bef = ort_h.at(trange[i-1]).observe(planet).apparent()
            now = ort_h.at(trange[i]).observe(planet).apparent()
            aft = ort_h.at(trange[i+1]).observe(planet).apparent()
            if(abs(AZ % 45) < abs(aft.altaz()[1].degrees % 45) and
               abs(AZ % 45) < abs(bef.altaz()[1].degrees % 45)):
                if (alt := now.altaz()[0].degrees) > 0:
                    ax.text(
                        t.utc_datetime(),
                        alt,
                        rich(AZ).replace("~", ""),
                        fontsize=15,
                        horizontalalignment='center',
                        c=col
                        )
                    m.append(i)

        ax.plot(
            dtrange,
            ort_h.at(trange).observe(planet).apparent().altaz()[0].degrees,
            marker="o",
            markevery=m,
            label=fname,
            c=col
            )
    pb.close()

    for i, t in enumerate(tqdm(trange[:-1], desc="Hintergrundfarbe")):
        sun_at_t = ort_h.at(t).observe(EPH["SUN"]).apparent()
        moon_at_t = ort_h.at(t).observe(EPH["MOON"]).apparent()
        for g in [-18, -12, -6, 0]:
            if sun_at_t.altaz()[0].degrees < g:
                ax.axvspan(
                    t.utc_datetime(),
                    trange[i+1].utc_datetime(),
                    facecolor='black',
                    alpha=0.6
                )
            if sun_at_t.altaz()[0].degrees > g:
                ax.axvspan(
                    t.utc_datetime(),
                    trange[i+1].utc_datetime(),
                    facecolor='#ffaa00',
                    alpha=0.1
                )
        if (
            moon_at_t.altaz()[0].degrees > 0 and
            sun_at_t.altaz()[0].degrees < -18
        ):
            ax.axvspan(
                t.utc_datetime(),
                trange[i+1].utc_datetime(),
                facecolor='cyan',
                alpha=0.1
            )

    ax.xaxis.set_ticks(
        [dt0+i*gridtick for i in range(int((dt1-dt0)/gridtick))]
    )
    ax.xaxis.set_ticklabels(
        [
            (dt0+i*gridtick).strftime("%Hh")
            for i in range(int((dt1-dt0)/gridtick))
        ]
    )
    ax.axis([dt0, dt1, 0, None])
    # ax.legend()
    ax.grid()
    plt.tight_layout()
    plt.savefig(
        f'{PATH}/tmp/{name}/Tagebogen-{dt0.strftime("%Y-%m-%d-%H")}.png'
    )


def tagebogen(ts0, lon: float, lat: float, elev: float, name: str):
    dt0 = ts0.utc_datetime().astimezone()
    dname = f"{PATH}/tmp/{name}/Tagebogen-{dt0.strftime('%Y-%m-%d-%H')}.png"
    if not os.path.exists(dname):
        ts1 = ts.from_datetime(dt0+timedelta(hours=24))
        print("Erstelle Tagebogen...")
        plot_tagebogen(ts0, ts1, lon, lat, elev, name)


###############################################################################
# SATELLITEN
###############################################################################


def draw_sat_überflug(ax: plt.axis, e, lon: float, lat: float, elev: float,
                      tick: timedelta = timedelta(seconds=15)) -> None:

    ort_g, ort_h = position(lon, lat, elev)

    ts_kul = e["Kulmination"]["ts"]
    sun_at_kul = ort_h.at(ts_kul).observe(EPH["SUN"]).apparent()
    moon_at_kul = ort_h.at(ts_kul).observe(EPH["MOON"]).apparent()

    tag = "#ffe0a5"
    colors = ["#665c49", "#282620", "#100f0e", "#060606"]

    color = tag
    for col, deg in zip(colors, [0, -6, -12, -18]):
        if sun_at_kul.altaz()[0].degrees < deg:
            color = col

    if (
        sun_at_kul.altaz()[0].degrees < -18 and
        moon_at_kul.altaz()[0].degrees > 0
    ):
        color = "#051f1f"

    ax.set_facecolor(color)

    for zeit in [
        e["Aufgang"]["dt"]+i*tick
        for i in range(int((e["Untergang"]["dt"] - e["Aufgang"]["dt"])/tick))
    ]:
        c = satcol(ts.from_datetime(zeit), e["satellite"], lon, lat, elev)
        ax.plot(
            [
                e["diff"].at(ts.from_datetime(z)).altaz()[1].degrees*pi/180
                for z in [zeit, zeit+tick]
            ],
            [
                e["diff"].at(ts.from_datetime(z)).altaz()[0].degrees
                for z in [zeit, zeit+tick]
            ],
            color=c, lw=5
        )


def draw_all_sats(events, lon: float, lat: float, elev: float,
                  name: str) -> None:
    plt.rcParams['figure.figsize'] = [4.0, 4.0]
    if not os.path.exists(f"{PATH}/tmp"):
        print("Erstelle Hilfsordner für die Bilder")
        os.mkdir(f"{PATH}/tmp")
    if not os.path.exists(f"{PATH}/tmp/{name}"):
        print("Erstelle Ordner für", name)
        os.mkdir(f"{PATH}/tmp/{name}")
    for i, e in tqdm(
            enumerate(events),
            desc="erzeuge Grafiken",
            total=len(events)
    ):
        fig = plt.figure()
        ax = fig.add_subplot(polar=True)
        ax.set_theta_zero_location("N")
        ax.set_theta_direction(-1)
        ax.set_ylim(90, 0)
        ax.set_yticks([0, 45])
        ax.set_yticklabels(["", ""])
        ax.set_xticks([0, np.pi/2, np.pi, np.pi*1.5])
        ax.set_xticklabels(["N", "O", "S", "W"])
        draw_sat_überflug(ax, e, lon, lat, elev, tick=timedelta(seconds=20))
        ax.grid()
        plt.tight_layout()
        plt.savefig(f"{PATH}/tmp/{name}/sat{e['index']}.png")
        plt.cla()


def satellite_events(satellites, ts0, ts1, min_degrees: float,
                     lon: float, lat: float, elev: float,
                     sun_deg: float = -9, max_mag: float = 4) -> None:

    ort_g, ort_h = position(lon, lat, elev)
    events = []
    index = 0
    for sat in tqdm(satellites, desc="suche Satelliten"):
        t, event = sat.find_events(ort_g, ts0, ts1, altitude_degrees=0)
        if len(event) < 3:
            continue
        while event[0] != 0:
            t = t[1:]
            event = event[1:]
        if len(event) < 3:
            continue
        while event[-1] != 2:
            t = t[:-1]
            event = event[:-1]
        for i in range(0, len(event), 3):
            if (sat-ort_g).at(t[1+i]).altaz()[0].degrees < min_degrees:
                continue
            e = dict()
            e["diff"] = (sat-ort_g)
            e["satellite"] = sat
            e["index"] = index

            if ort_h.at(
                t[i+1]
            ).observe(EPH["SUN"]).apparent().altaz()[0].degrees > sun_deg:
                continue

            for j, name in enumerate(["Aufgang", "Kulmination", "Untergang"]):
                s = dict()
                s["ts"] = t[i+j]
                s["dt"] = t[i+j].utc_datetime()
                s["mag"] = mag(s["ts"], e["satellite"], lon, lat, elev)
                s["altaz"] = e["diff"].at(s["ts"]).altaz()
                s["Distanz"] = e["diff"].at(s["ts"]).distance()
                s["Geschwindigkeit"] = e["diff"].at(s["ts"]).speed()
                s["Name"] = name
                e[name] = s

            for name, test, delta in zip(
                ["Erscheint", "Verschwindet"],
                ["Aufgang", "Untergang"],
                [timedelta(seconds=1), timedelta(seconds=-1)]
            ):
                if not e["diff"].at(e[test]["ts"]).is_sunlit(EPH):
                    ti = e[test]["ts"].utc_datetime()
                    abbr = False
                    while not e["diff"].at(
                        ts.from_datetime(ti)
                    ).is_sunlit(EPH):
                        ti += delta
                        if (
                            ti > e["Untergang"]["dt"]
                            or
                            ti < e["Aufgang"]["dt"]
                        ):
                            abbr = True
                            break
                    if abbr:
                        continue

                    s = dict()
                    s["dt"] = ti
                    s["ts"] = ts.from_datetime(ti)
 
                    m = mag(
                        s["ts"],
                        e["satellite"],
                        lon, lat, elev
                    )
                    if isinstance(m, float):
                        s["mag"] = m
                    else:
                        s["mag"] = None
                    s["altaz"] = e["diff"].at(s["ts"]).altaz()
                    s["mag"] = mag(s["ts"], e["satellite"], lon, lat, elev)
                    s["Distanz"] = e["diff"].at(s["ts"]).distance()
                    s["Geschwindigkeit"] = e["diff"].at(s["ts"]).speed()
                    s["Name"] = name

                    e[name] = s

            if min(
                [
                    (e[s]["mag"] if isinstance(e[s]["mag"], float) else 99)
                    if isinstance(e[s], dict) else 99
                    for s in e
                ]
            ) < max_mag:
                events.append(e)
                index += 1
    print("GEFUNDEN: ", len(events))
    return events

###############################################################################
# HTML
###############################################################################


def elongation(ts0, ts1, lon: float, lat: float, elev: float):
    ret = list()
    ort_g, ort_h = position(lon, lat, elev)
    for pname in ["Venus", "Mercury"]:
        def elongation_at(t):
            e = EPH["EARTH"].at(t)
            s = e.observe(EPH["SUN"]).apparent()
            p = e.observe(EPH[pname]).apparent()
            return s.separation_from(p).degrees
        elongation_at.step_days = 15

        t, y = searchlib.find_maxima(ts0, ts1, elongation_at)
        for ti, yi in zip(t, y):
            try:
                m = planetary_magnitude(ort_h.at(ti).observe(EPH[f'{pname}']))
                m = "<b>"+str(runde(m, 1, 4))+" mag</b>"
            except:
                m = "~~?~~mag"
            alt, az, ra, dec, dis = AltAzRaDecDis(
                                        EPH[pname], ti, lon, lat, elev
            )
            e = EPH["EARTH"].at(ti)
            sun = e.observe(EPH["SUN"]).apparent()
            _, slon, _ = sun.frame_latlon(ecliptic_frame)
            planet = e.observe(EPH[pname]).apparent()
            _, plon, _ = planet.frame_latlon(ecliptic_frame)
            is_east = (plon.degrees-slon.degrees) % 360 < 180
            direction = "östlich" if is_east else "westlich"
            d = dict()
            phase = planetenphase(ti, pname)
            d["dt"] = ti.utc_datetime()
            d["html"] = html_row(
                ti.utc_datetime().strftime("%Y-%m-%d<br>%Hh%Mm%Ss"),
                planet_darstell(pname),
                (f"<b>{direction}e~Elongation:~{round(yi, 1)}º</b>"
                 f"<br>Phase:~{runde(phase, 1, 5)}º~~"
                 f"Beleuchtet:~{runde(min(phase, 360-phase)/1.8, 1, 5)}%~~"
                 f"{m}<br>RA:~{ra}~~DEC:~{dec}")
            )
            ret.append(d)
    return ret


def konjunktion_opposition(ts0, ts1, lon: float, lat: float, elev: float):
    ret = list()
    ort_g, ort_h = position(lon, lat, elev)
    for pname in [
        "Mercury",
        "Venus",
        "Mars",
        "Jupiter_barycenter",
        "Saturn_barycenter",
        "Uranus_barycenter",
        "Neptune_barycenter"
    ]:
        f = almanac.oppositions_conjunctions(EPH, EPH[pname])
        t, y = almanac.find_discrete(ts0, ts1, f)
        for ti, yi in zip(t, y):
            try:
                m = planetary_magnitude(ort_h.at(ti).observe(EPH[f'{pname}']))
                m = "<b>"+str(runde(m, 1, 4))+" mag</b>"
            except:
                m = "~~?~~mag"
            alt, az, ra, dec, dis = AltAzRaDecDis(
                EPH[pname], ti, lon, lat, elev
            )
            phase = planetenphase(ti, pname)
            if pname in ["Venus", "Mercury"]:
                ereignis = (
                    "obere Konjunktion" if yi == 1 else "untere Konjunktion"
                )
            else:
                ereignis = ("Opposition~" if yi == 1 else "Konjunktion")
            d = dict()
            d["dt"] = ti.utc_datetime()
            d["html"] = html_row(
                ti.utc_datetime().strftime("%Y-%m-%d<br>%Hh%Mm%Ss"),
                planet_darstell(pname),
                (f"<b>{ereignis}</b><br>Phase:~{runde(phase, 1, 5)}º~~"
                 f"Beleuchtet:~{runde(min(phase, 360-phase)/1.8, 1, 5)}%~~"
                 f"{m}<br>RA:~{ra}~~DEC:~{dec}")
            )
            ret.append(d)
    return ret


def tagebogen_html(ts0, ts1, lon: float, lat: float, elev: float, name: str):
    dt0 = ts0.utc_datetime().astimezone()
    dt1 = ts1.utc_datetime().astimezone()

    dts = dt0.replace(hour=12, minute=0, second=0)
    ret = list()
    t = dts
    leg = ""
    for p in [
        "Sun", "Moon", "Mercury", "Venus", "Mars",
        "Jupiter", "Saturn", "Uranus", "Neptune"
    ]:
        leg += f'<div style="color:{planet_col(p)}">{p}</div>'
    leg += ""
    while t < dt1:
        s = ts.from_datetime(t)
        tagebogen(s, lon, lat, elev, name)
        h = dict()
        h["dt"] = t
        h["html"] = html_row(
            t.strftime("%Hh%Mm"),
            f"Tagebogen<br>diagramm<br>{leg}",
            (f'<img src="{PATH}/tmp/{name}/'
             f'Tagebogen-{t.strftime("%Y-%m-%d-%H")}.png" height="300">')
        )
        ret.append(h)
        t += timedelta(hours=24)
    return ret


def moon_events(ts0, ts1, lon: float, lat: float, elev: float):
    def altF(ts):
        alt, az, ra, dec, dis = AltAzRaDecDis(EPH["MOON"], ts, lon, lat, elev)
        return alt.degrees
    altF.step_days = 0.5

    ort_g, ort_h = position(lon, lat, elev)

    ret = []
    ta = list()
    ya = list()
    # PHASEN
    t, y = almanac.find_discrete(ts0, ts1, almanac.moon_phases(EPH))
    ta += t
    ya += [almanac.MOON_PHASES[yi] for yi in y]
    # NODES
    t, y = almanac.find_discrete(ts0, ts1, almanac.moon_nodes(EPH))
    ta += t
    ya += [almanac.MOON_NODES[yi] for yi in y]
    # AUF UNTER
    t, y = almanac.find_discrete(
            ts0,
            ts1,
            almanac.risings_and_settings(EPH, EPH['MOON'], ort_g)
    )
    ta += t
    n = ["Untergang~~", "Aufgang~~~~"]
    ya += [n[yi] for yi in y]
    # Kulmination
    t, y = searchlib.find_maxima(ts0, ts1, altF)
    ta += t
    ya += ["Kulmination" for yi in y]
    for ti, yi in zip(ta, ya):
        alt, az, ra, dec, dis = AltAzRaDecDis(EPH["MOON"], ti, lon, lat, elev)
        phase = planetenphase(ti, "MOON")
        k = yi == "Kulmination"
        this = dict()
        this["dt"] = ti.utc_datetime()
        this["html"] = html_row(
            ptime(ti.utc_datetime()),
            moon_darstell(ti),
            f"<b>{yi}</b>~~az:~{runde(az.degrees, 0, 3)}º~"
            f"{rich(az.degrees)}~~{'<b>' if k else ''}alt:~"
            f"{runde(alt.degrees, 0, 3)}º{'</b>' if k else ''}<br>"
            f"Phase:~{runde(phase, 1, 5)}º~~"
            f"Beleuchtet:~{runde(min(phase, 360-phase)/1.8, 1, 5)}%<br>"
            f"RA:~{ra}~~DEC:~{dec}"
        )
        ret.append(this)

    return ret


def planeten_events(ts0, ts1, lon: float, lat: float, elev: float):
    def altF(t) -> float:
        return ort_h.at(t).observe(EPH[planet]).apparent().altaz()[0].degrees
    altF.step_days = 0.5

    ort_g, ort_h = position(lon, lat, elev)
    ret = []

    for planet in [
        "MERCURY",
        "VENUS",
        "MARS",
        "JUPITER_BARYCENTER",
        "SATURN_BARYCENTER",
        "URANUS_BARYCENTER",
        "NEPTUNE_BARYCENTER"
    ]:
        t = list()
        y = list()

        ta, ya = almanac.find_discrete(
            ts0,
            ts1,
            almanac.risings_and_settings(EPH, EPH[f'{planet}'], ort_g)
        )

        n = ["Untergang~~", "Aufgang~~~~"]
        t += ta
        y += [n[yi] for yi in ya]

        ta, ya = searchlib.find_maxima(ts0, ts1, altF)
        t += ta
        y += ["Kulmination" for yi in ya]

        for ti, yi in zip(t, y):
            phase = planetenphase(ti, planet)
            try:
                m = planetary_magnitude(ort_h.at(ti).observe(EPH[f'{planet}']))
                m = "<b>"+str(runde(m, 1, 4))+" mag</b>"
            except:
                m = "~~?~~mag"
            alt, az, ra, dec, dis = AltAzRaDecDis(
                EPH[planet], ti, lon, lat, elev
            )
            k = yi == "Kulmination"
            this = dict()
            this["dt"] = ti.utc_datetime()
            this["html"] = html_row(
                ptime(ti.utc_datetime()),
                planet_darstell(planet),
                f"<b>{yi}</b>~~az:~{runde(az.degrees, 0, 3)}º~"
                f"{rich(az.degrees)}~~{'<b>' if k else ''}"
                f"alt:~{runde(alt.degrees, 0, 3)}º{'</b>' if k else ''}<br>"
                f"Phase:~{runde(phase, 1, 5)}º~~"
                f"Beleuchtet:~{runde(min(phase, 360-phase)/1.8, 1, 5)}%~~"
                f"{m}<br>RA:~{ra}~~DEC:~{dec}"
            )
            ret.append(this)
    return ret


def sun_events(ts0, ts1, lon: float, lat: float, elev: float):
    dämmerungen = [
        "Nacht", "Astronomische~Dämmerung", "Nautische~Dämmerung",
        "Bürgerliche~Dämmerung", "Tag"
    ]
    dämbesch = [
        [
            "", "Es~ist~maximal~Dunkel", "Sternenbilder~werden~sichtbar",
            "helle~Sterne~und~Planeten~tauchen~auf", ""
        ],
        [
            "", "schwache~Sterne~verblassen", "Sternenbilder~lösen~sich~auf",
            "helle~Sterne~und~Planeten~verblassen", ""
        ]
    ]
    ort_g, ort_h = position(lon, lat, elev)
    ret = []

    f = almanac.dark_twilight_day(EPH, ort_g)
    times, events = almanac.find_discrete(ts0, ts1, f)

    previous_e = f(ts0).item()
    for t, e in zip(times, events):
        alt, az, ra, dec, dis = AltAzRaDecDis(EPH["Sun"], t, lon, lat, elev)
        this = dict()
        this["dt"] = t.utc_datetime()
        this["html"] = "<tr>"
        if previous_e < e:  # Aufgang
            if e >= 4:
                this["html"] = html_row(
                    ptime(t.utc_datetime()),
                    f"{sun_darstell(e-1)}",
                    f"<b>Aufgang</b>~~~~~~az:~{runde(az.degrees, 0, 3)}º~"
                    f"{rich(az.degrees)}<br>RA:~{ra}~~DEC:~{dec}"
                )
            else:
                this["html"] = html_row(
                    ptime(t.utc_datetime()),
                    f"{sun_darstell(e-1)}",
                    f"<b>{dämmerungen[e]}</b>~~{dämbesch[1][e]}<br>alt:~"
                    f"{runde(alt.degrees, 0, 3)}º"
                )

        else:  # Untergang
            if e >= 3:
                this["html"] = html_row(
                    ptime(t.utc_datetime()),
                    f"{sun_darstell(e)}",
                    f"<b>Untergang</b>~~~~az:~{runde(az.degrees, 0, 3)}º~"
                    f"{rich(az.degrees)}<br>RA:~{ra}~~DEC:~{dec}"
                )
            else:
                this["html"] = html_row(
                    ptime(t.utc_datetime()),
                    f"{sun_darstell(e)}",
                    f"<b>{dämmerungen[e+1]}</b>~~{dämbesch[0][e+1]}<br>"
                    f"alt:~{runde(alt.degrees, 0, 3)}º"
                )
        previous_e = e

        this["html"] += "</tr>"
        ret.append(this)
    return ret


def sat_events_to_html(events, lon: float, lat: float, elev: float,
                       sat_mag: float, name: str) -> str:
    rows = list()
    ort_g, ort_h = position(lon, lat, elev)
    for i, ev in enumerate(events):
        parts = list()
        for k, e in ev.items():
            if isinstance(e, dict):
                sl = (
                    ev["diff"].at(e["ts"]).is_sunlit(EPH) or
                    e["Name"] in ["Erscheint", "Verschwindet"]
                )
                nicht_horizont = not e["Name"] in ["Aufgang", "Untergang"]
                m = e["mag"]
                hell = False
                if isinstance(m, float):
                    m_str = runde(m, 1, 4)
                    hell = m < sat_mag
                else:
                    m_str = "~?~~"
                if sl:
                    parts.append(dict())
                else:
                    continue
                parts[-1]["dt"] = e["dt"]
                parts[-1]["text"] = (
                    f"<b>{e['Name']}{SPACE*(13-len(e['Name']))}"
                    f"{e['dt'].strftime('%Hh%Mm%Ss')}"
                    f"{'</b>' if not hell else ''}~~"
                    f"{m_str}mag{'</b>' if hell else ''}~"
                    f"az:{runde(e['altaz'][1].degrees, 0, 4)}º~"
                    f"{rich(e['altaz'][1].degrees)}~~"
                )
                if nicht_horizont:
                    parts[-1]["text"] += (
                        f"{'<b>' if e['Name'] == 'Kulmination' else ''}"
                        f"h:~{runde(e['altaz'][0].degrees, 0, 3)}º"
                        f"{'</b>' if e['Name'] == 'Kulmination' else ''}"
                    )
                if e["Name"] == "Kulmination":
                    parts[-1]["text"] += (
                        f"<br>~~Distanz:~{round(e['Distanz'].km, 1)}km~~"
                        f"Geschw.:~"
                        f"{round(e['Geschwindigkeit'].km_per_s, 1)}km/s~~"
                    )
                parts[-1]["text"] += "<br>"

        parts.sort(key=lambda x: x["dt"])

        rows.append(dict())
        rows[i]["dt"] = ev['Kulmination']["dt"]
        rows[i]["html"] = html_row(
            ptime(rows[i]["dt"]),
            sat_darstell(ev["satellite"]),
            f'<img src="{PATH}/tmp/{name}/sat{ev["index"]}.png"'
            f'height="90" align="right">'
            f'{"".join(part["text"] for part in parts)}'
        )
    return rows

###############################################################################
##
###############################################################################


def calsky(dt0, dt1, lon: float, lat: float, elev: float,
           name: str = "table", sat=None, sat_mag=4, tageb=False):

    print(f"rechne {name} bei {lat}|{lon}")
    if not os.path.exists(f"{PATH}/tmp"):
        os.mkdir(f"{PATH}/tmp")
    if not os.path.exists(f"{PATH}/tmp/{name}"):
        os.mkdir(f"{PATH}/tmp/{name}")

    ts0, ts1 = ts.from_datetime(dt0), ts.from_datetime(dt1)
    tab = list()
    if tageb:
        tab += tagebogen_html(ts0, ts1, lon, lat, elev, name)
    if sat:
        sats = satellite_events(
            sat, ts0, ts1, 20, lon, lat, elev, -6, sat_mag
        )
        draw_all_sats(sats, lon, lat, elev, name)
        tab += sat_events_to_html(sats, lon, lat, elev, sat_mag, name)

    tab += moon_events(ts0, ts1, lon, lat, elev)
    tab += planeten_events(ts0, ts1, lon, lat, elev)
    tab += sun_events(ts0, ts1, lon, lat, elev)
    tab += konjunktion_opposition(ts0, ts1, lon, lat, elev)
    tab += elongation(ts0, ts1, lon, lat, elev)

    tab.sort(key=lambda x: x["dt"])

    table_head = (
        '<table style="font-family: monospace; width: 80%;margin-left:auto;'
        'margin-right:auto"><tr>'
        '<th witdh="15%">Zeit</th>'
        '<th width = "15%">Objekt</th>'
        '<th>Beschreibung</th></tr>'
    )

    def table_caption(zeit: datetime) -> str:
        return f'<caption><h3>{zeit.strftime("%A, %Y-%m-%d")}</h3></caption>'

    html = f'''
        <!DOCTYPE html>
        <head>
        <title>SkyCalc.py</title>
        <link rel="shortcut icon" type="image/x-icon"
            href = "{PATH}/favicon.png">
        <style>
        table, th, td{{border: 1px solid black;border-collapse: collapse;}}
        td{{padding: 5px; text-align: left;}}
        </style>
        </head>
        <body>
        <h1>Astronomische Ereignisse in
            {name}{round(lat, 2)}ºN, {round(lon, 2)}ºE)</h1>
        <h3>{dt0.strftime("%A, %Y-%m-%d %Hh")}
        bis {dt1.strftime("%A, %Y-%m-%d %Hh")}</h3>
    '''
    # ORTSLISTE
    html += "<ul>"
    for o in listOrte():
        html += f'<li><a href="{o}.html">{o}</a></li>'
    html += "</ul>"
    # /ORTSLISTE
    html += (
        f"Errechnet {datetime.now().strftime('%Y-%m-%d um %Hh%Mm%Ss')}, "
        f"Zeiten in {dt0.tzinfo}")
    html += f"{table_head}"
    html += f"{table_caption(dt0)}"
    for i, t in enumerate(tab):
        html += t["html"]
        if ((tab[min((i+1), len(tab)-1)]["dt"]).astimezone().day
                != (t["dt"]).astimezone().day):
            html += "</table>"
            html += f"{table_head}"
            html += f'{table_caption(tab[(i+1)%len(tab)]["dt"])}'
    html += "</table></body>"

    if not os.path.exists(f"{PATH}/html"):
        os.mkdir(f"{PATH}/html")
    with open(f"{PATH}/html/{name}.html", "w") as f:
        f.write(html)
    print("Fertig")

    return html


def readConfig():
    conf = f"{PATH}/.config"
    hilfe = (
        "https://gitlab.com/Bacaliu/skycalc/"
        "-/blob/main/README.md#konfigurationsdatei"
    )
    if not os.path.exists(conf):
        with open(conf, "w") as f:
            f.write("[Example]\nlon = 0.00\nlat = 0.00\nelev = 0")
        print(
            "Kein .config-file.",
            f"Bitte in „{conf}“ die Daten des Wohnortes angeben",
            f"Details: {hilfe}"
        )
    with open(f"{PATH}/.config", "r") as f:
        w = f.read().split("[")
    for wi in w:
        if len(wi) == 0:
            continue
        name = wi.split("]")[0]
        lon = 0
        lat = 0
        elev = 0
        sat_m = 5
        for line in wi.split("\n"):
            ll = line.split()
            if "lon" in ll:
                lon = float(ll[-1])
            if "lat" in ll:
                lat = float(ll[-1])
            if "elev" in ll:
                elev = float(ll[-1])
            if "sat-m" in ll:
                sat_m = float(ll[-1])

        yield lon, lat, elev, name, sat_m


def listOrte():
    with open(f"{PATH}/.config", "r") as f:
        w = f.read().split("[")
    for wi in w:
        o = wi.split("]")[0]
        if o != "":
            yield o


def main():
    sat = (VISUAL if "-sat" in sys.argv else False)
    dur = 24
    ort = None
    tageb = False
    start = datetime.now().astimezone().replace(second=0, minute=0)
    for i, arg in enumerate(sys.argv):
        if arg == "-dur":
            dur = int(sys.argv[i+1])
        if arg == "-start":
            start = datetime.strptime(
                sys.argv[i+1],
                "%Y-%m-%d-%H"
            ).astimezone()
        if arg == "-ort":
            ort = sys.argv[i+1]
        if arg == "-tb":
            tageb = True

    plt.rcParams['figure.max_open_warning'] = 200  # Warnung unterdrücken
    for lon, lat, elev, name, sat_mag in readConfig():
        if ort is None or ort == name:
            calsky(
                start, start+timedelta(hours=dur),
                lon, lat, elev, name, sat,
                sat_mag=sat_mag, tageb=tageb
            )


if __name__ == "__main__":
    main()
